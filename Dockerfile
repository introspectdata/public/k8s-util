FROM python:3.6-alpine

# see: https://storage.googleapis.com/kubernetes-release/release/stable.txt
ARG KUBECTL_VERSION=v1.13.5
ARG HELM_VERSION=v2.14.1

ADD https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl /usr/local/bin/kubectl
ADD https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz /tmp/helm.tar.gz

RUN apk add --no-cache curl git bash ca-certificates zip tar openssh-client && \
    tar xvf /tmp/helm.tar.gz -C /usr/local/bin/ --strip 1 && \
    chmod +x /usr/local/bin/helm && \
    chmod +x /usr/local/bin/kubectl && \
    rm -rf /var/lib/apt/lists/*

